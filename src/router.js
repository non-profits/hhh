import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/about-founder",
      name: "aboutFounder",
      component: () => 
        import("./views/AboutFounder.vue")
    },
    {
      path: "/credits",
      name: "credits",
      components: () => 
        import("./views/Credits.vue")
    },
    {
      path: "/contact",
      name: "contact",
      components: () =>
        import("./views/Contact.vue")
    },
    {
      path: "/events/:id",
      name: "viewEvent",
      component: () => 
        import("./views/Event.vue")
    }
  ]
});
