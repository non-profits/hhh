# Humanity Helping Hands

## Project setup

```bash
npm install
```

### Compiles and hot-reloads for development

```bash
npm run serve
```
